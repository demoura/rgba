#!/bin/bash
export RUSTFLAGS="-Z print-link-args"

cargo +nightly build \
    -Z build-std=core,alloc \
    -Z extra-link-arg \
    --target thumbv4t-none-eabi.json \
    -p rgba --no-default-features --features gba --release &&
arm-none-eabi-objcopy -O binary target/thumbv4t-none-eabi/release/rgba rgba.gba &&
gbafix rgba.gba -t"Adven Rust"
