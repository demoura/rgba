// rgba-assets <https://gitlab.com/demoura/rgba>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use adven::{
	math::{fixed::types::I16F16, Vector2},
};
use adven_ecs::{storage::HashmapStorage, Component};

/////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

#[repr(transparent)]
#[derive(Clone, Copy, Eq, PartialEq)]
#[cfg_attr(debug_assertions, derive(Debug))]
#[cfg_attr(feature = "export", derive(serde::Deserialize, serde::Serialize))]
pub struct Position(pub Vector2<I16F16>);

#[repr(transparent)]
#[derive(Clone, Copy, Eq, PartialEq)]
#[cfg_attr(debug_assertions, derive(Debug))]
#[cfg_attr(feature = "export", derive(serde::Deserialize, serde::Serialize))]
pub struct Rotation(pub I16F16);

#[repr(transparent)]
#[derive(Clone, Copy, Eq, PartialEq)]
#[cfg_attr(debug_assertions, derive(Debug))]
#[cfg_attr(feature = "export", derive(serde::Deserialize, serde::Serialize))]
pub struct Scale(pub Vector2<I16F16>);

#[repr(transparent)]
#[derive(Clone, Copy, PartialEq)]
#[cfg_attr(debug_assertions, derive(Debug))]
#[cfg_attr(feature = "export", derive(serde::Deserialize, serde::Serialize))]
pub struct Velocity(pub Vector2<I16F16>);

#[repr(transparent)]
#[derive(Clone, Copy, PartialEq)]
#[cfg_attr(debug_assertions, derive(Debug))]
#[cfg_attr(feature = "export", derive(serde::Deserialize, serde::Serialize))]
pub struct AngularVelocity(pub I16F16);

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

impl Component for Position {
	type Storage = HashmapStorage<Self>;
}

impl Component for Rotation {
	type Storage = HashmapStorage<Self>;
}

impl Component for Scale {
	type Storage = HashmapStorage<Self>;
}

impl Component for Velocity {
	type Storage = HashmapStorage<Self>;
}

impl Component for AngularVelocity {
	type Storage = HashmapStorage<Self>;
}

#[cfg(feature = "export")]
impl adven::repr_writer::ReprWritable for Position {
	fn align(abi: &adven::repr_writer::ReprAbi) -> usize
	where
		Self: Sized,
	{
		Vector2::align(abi)
	}

	fn align_dyn(&self, abi: &adven::repr_writer::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(
		&self,
		writer: &mut dyn adven::repr_writer::ReprWriter,
	) -> adven::repr_writer::Result<()> {
		self.0.write(writer)
	}
}

#[cfg(feature = "export")]
impl adven::repr_writer::ReprWritable for Rotation {
	fn align(abi: &adven::repr_writer::ReprAbi) -> usize
	where
		Self: Sized,
	{
		I16F16::align(abi)
	}

	fn align_dyn(&self, abi: &adven::repr_writer::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(
		&self,
		writer: &mut dyn adven::repr_writer::ReprWriter,
	) -> adven::repr_writer::Result<()> {
		self.0.write(writer)
	}
}

#[cfg(feature = "export")]
impl adven::repr_writer::ReprWritable for Scale {
	fn align(abi: &adven::repr_writer::ReprAbi) -> usize
	where
		Self: Sized,
	{
		Vector2::align(abi)
	}

	fn align_dyn(&self, abi: &adven::repr_writer::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(
		&self,
		writer: &mut dyn adven::repr_writer::ReprWriter,
	) -> adven::repr_writer::Result<()> {
		self.0.write(writer)
	}
}

#[cfg(feature = "export")]
impl adven::repr_writer::ReprWritable for Velocity {
	fn align(abi: &adven::repr_writer::ReprAbi) -> usize
	where
		Self: Sized,
	{
		Vector2::align(abi)
	}

	fn align_dyn(&self, abi: &adven::repr_writer::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(
		&self,
		writer: &mut dyn adven::repr_writer::ReprWriter,
	) -> adven::repr_writer::Result<()> {
		self.0.write(writer)
	}
}

#[cfg(feature = "export")]
impl adven::repr_writer::ReprWritable for AngularVelocity {
	fn align(abi: &adven::repr_writer::ReprAbi) -> usize
	where
		Self: Sized,
	{
		I16F16::align(abi)
	}

	fn align_dyn(&self, abi: &adven::repr_writer::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(
		&self,
		writer: &mut dyn adven::repr_writer::ReprWriter,
	) -> adven::repr_writer::Result<()> {
		self.0.write(writer)
	}
}
