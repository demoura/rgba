// rgba-assets <https://gitlab.com/demoura/rgba>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#![cfg_attr(not(feature = "std"), no_std)]
#![warn(clippy::all)]

////////////////////////////////////////////////////////////////////////////////
// Modules
////////////////////////////////////////////////////////////////////////////////

#[cfg(feature = "export")]
pub mod export;
pub mod components;

////////////////////////////////////////////////////////////////////////////////
// Re-Exports
////////////////////////////////////////////////////////////////////////////////

#[cfg(feature = "export")]
pub use adven_assets;

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use adven::{display::SpriteAsset, math::fixed::types::I16F16};
use components::{Position, Velocity, Rotation, Scale, AngularVelocity};

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

#[repr(C)]
#[cfg_attr(feature = "debug_trait", derive(Debug))]
pub struct Ship {
	pub sprite: &'static SpriteAsset<'static>,
	pub velocity: I16F16,
	pub max_health: u32,
	pub damage: u32,
	pub angular_velocity: I16F16,
}

pub struct ShipEntity {
	position: Position,
	rotation: Rotation,
	scale: Scale,
	velocity: Velocity,
	angular_velocity: AngularVelocity,


}

impl<C: Component> SerdeComponent for C {
}

pub struct SerdeEntityTemplate(Vec<Box<dyn SerdeComponent>>);

// pub struct ArtacTileMetadata {
// 	pub movementCost: u32,
// 	pub terrainType: TerrainType
// }
