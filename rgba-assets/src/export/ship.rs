////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use adven::{
	math::fixed::types::I16F16,
	repr_writer::{ReprAbi, ReprWritable, ReprWriter},
};
use adven_assets::{assets::Sprite, typetag, Asset, AssetId, ExportableAsset, SerdeAsset};
use serde::{self as serde, Deserialize, Serialize};

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

#[derive(Clone, Deserialize, Serialize)]
pub struct Ship {
	pub sprite: AssetId<Sprite>,
	pub velocity: f32,
	pub max_health: u32,
	pub damage: u32,
	pub angular_velocity: f32,
}

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

impl Asset for Ship {
	fn as_any(&self) -> &dyn core::any::Any {
		self
	}

	fn as_exportable_asset(&self) -> Option<&dyn ExportableAsset> {
		Some(self)
	}
}

#[typetag::deserialize(name = "rgba::Ship")]
impl SerdeAsset for Ship {
	fn into_asset(self: Box<Self>) -> Box<dyn Asset> {
		self
	}
}

impl ExportableAsset for Ship {
	fn type_name(&self) -> &'static str {
		"rgba_assets::Ship"
	}

	fn as_repr_writable(&self) -> &dyn adven::repr_writer::ReprWritable {
		self
	}
}

impl ReprWritable for Ship {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized,
	{
		*[abi.align_of_ptr, u32::align(abi)].iter().max().unwrap()
	}

	fn align_dyn(&self, abi: &ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn ReprWriter) -> adven::repr_writer::Result<()> {
		let align = Self::align(writer.abi());
		let seq = writer.start_struct(align)?;
		seq.write_element(&self.sprite)?;
		seq.write_element(&I16F16::from_num(self.velocity).to_bits())?;
		seq.write_element(&self.max_health)?;
		seq.write_element(&self.damage)?;
		seq.write_element(&I16F16::from_num(self.angular_velocity).to_bits())?;
		seq.end()
	}
}
