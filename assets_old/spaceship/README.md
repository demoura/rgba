# Space ship simple assets by pixelalek
https://pixelalek.itch.io/spaceship-simple-assets

## LICENSE

You can modify and use it on free or commercial projects. You don't need to put credits, but if you want to help me I will appreciate. You may not redistribute it or resell it.
