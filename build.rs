// rgba <https://gitlab.com/demoura/rgba>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::{env, marker::PhantomData, path::PathBuf, str::FromStr};

use rgba_assets::adven_assets::{abi::{GbaEndian, TargetSpec, TargetTriple, ARM_V4T_ABI}, AssetPipeline};

pub const PKG_NAME: &'static str = env!("CARGO_PKG_NAME");
pub const PKG_DESCR: &'static str = env!("CARGO_PKG_DESCRIPTION");
pub const PKG_VERSION: &'static str = env!("CARGO_PKG_VERSION");
pub const PKG_AUTHORS: &'static str = env!("CARGO_PKG_AUTHORS");
pub const TIME_FORMAT: &'static str = "%Y-%m-%d %H:%M:%S.%f";

fn main() {
	init_logger().expect("Could not initialize logger");

	log::info!("{} - {}", PKG_NAME, PKG_DESCR);
	log::info!("Version {}", PKG_VERSION);
	log::info!("");
	log::info!("If you have any complaints go yell them to:");
	PKG_AUTHORS
		.split(':')
		.for_each(|name| log::info!("\t* {}", name));
	log::info!("");

	let manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
	let target = env::var("TARGET").unwrap();
	let out_dir = env::var("OUT_DIR").unwrap();

	let target_spec = TargetSpec::<'_, GbaEndian> {
		triple: TargetTriple::from_str(&target).unwrap(),
		abi: &ARM_V4T_ABI,
		_byteorder: PhantomData,
	};

	let input_directory: PathBuf = [&manifest_dir, "assets"].iter().collect();
	let output_directory = PathBuf::from(out_dir);

	AssetPipeline::new(input_directory, output_directory, target_spec)
		.build()
		.unwrap();

	println!("cargo:rustc-link-arg-bins=-Tadven-bootloader.ld");
	println!("cargo:rustc-link-arg-bins=-lcrt0");
	println!("cargo:rerun-if-changed=assets/");
	println!("cargo:rerun-if-changed=build.rs");
}

fn init_logger(/*config: &config::Config*/) -> Result<(), log::SetLoggerError> {
	let level = log::Level::Trace;
	//let level = config.logging.level.clone();
	//let tgtbl = config.logging.target_prefix_blacklist.clone();

	fern::Dispatch::new()
		.format(move |fmt, message, record| {
			fmt.finish(format_args!(
				"[{}][{}][{}] {}",
				chrono::Local::now().format(TIME_FORMAT),
				record.level(),
				// color.color(record.level()),
				record.target(),
				message
			))
		})
		.filter(move |meta| meta.level() <= level)
		// .filter(move |meta|
		//     tgtbl.iter()
		//         .skip_while(|blacklisted|{
		//                 !meta.target().starts_with(blacklisted.as_str())
		//         })
		//         .next()
		//         .is_none()
		// )
		.level_for("mio", log::LevelFilter::Info)
		.level_for("tokio_core", log::LevelFilter::Info)
		.chain(fern::log_file("adven-assets.log").unwrap())
		.apply()
}
