#!/bin/bash
export RUSTFLAGS="-Z print-link-args -Clink-arg=-Tadven-bootloader.ld"

cargo +nightly build -Z build-std=core,alloc --target thumbv4t-devkitarm-eabi.json -p rgba &&
arm-none-eabi-objcopy -O binary target/thumbv4t-devkitarm-eabi/debug/rgba rgba.gba &&
gbafix rgba.gba -t"Adven Rust"
