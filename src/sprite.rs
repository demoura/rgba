// rgba <https://gitlab.com/demoura/rgba>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use alloc::{rc::Rc, vec::Vec};
use core::{alloc::Layout, any::TypeId};

use adven::{
	display::*,
	math::{
		busize,
		fixed::types::I8F8,
		Matrix3, Vector2,
	},
	volatile::Volatile,
};
use adven_allocators::{BuddyAlloc, RawAlloc, RcAlloc};
use adven_ecs::{storage::HashmapStorage, Component, Resources, System, World};
use hashbrown::HashMap;

use crate::transform::Position;

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

#[derive(Clone, Copy)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Sprite {
	pub asset: &'static SpriteAsset<'static>,
	pub flip_mode: FlipMode,
	pub palette_bank: PaletteBankIdx,
}

pub struct SpriteAffine {
	pub asset: &'static SpriteAsset<'static>,
	pub palette_bank: PaletteBankIdx,
	pub affine_matrix: Matrix3<I8F8>,
}

type SpriteAlloc<'a> = RcAlloc<BuddyAlloc<RawAlloc<'a>>>;

pub enum SpriteMemory<'a> {
	Color4Bit(Vec<Tile4bpp, SpriteAlloc<'a>>),
	Color8Bit(Vec<Tile8bpp, SpriteAlloc<'a>>),
}

pub struct SpriteRenderer<'a, C: SpriteControl> {
	sprite_control: C,
	sprite_alloc: SpriteAlloc<'a>,
	sprites: HashMap<usize, SpriteMemory<'a>>,
}

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

impl Component for Sprite {
	type Storage = HashmapStorage<Self>;
}

impl Component for SpriteAffine {
	type Storage = HashmapStorage<Self>;
}

impl<'a> SpriteMemory<'a> {
	pub fn new(tiles: TilesRef<'_>, sprite_alloc: SpriteAlloc<'a>) -> Self {
		match tiles {
			TilesRef::Color4Bit(tiles) => {
				let mut sprite_img = Vec::new_in(sprite_alloc);

				sprite_img.extend_from_slice(&*tiles);

				SpriteMemory::Color4Bit(sprite_img)
			}
			TilesRef::Color8Bit(tiles) => {
				let mut sprite_img = Vec::new_in(sprite_alloc);

				sprite_img.extend_from_slice(&*tiles);

				SpriteMemory::Color8Bit(sprite_img)
			}
		}
	}

	pub fn color_mode(&self) -> ColorMode {
		match self {
			Self::Color4Bit(_) => ColorMode::Color4Bit,
			Self::Color8Bit(_) => ColorMode::Color8Bit,
		}
	}

	pub fn len(&self) -> usize {
		match self {
			Self::Color4Bit(tiles) => tiles.len(),
			Self::Color8Bit(tiles) => tiles.len(),
		}
	}

	pub fn base_tile(&self) -> usize {
		let ptr = match self {
			Self::Color4Bit(tiles) => tiles.as_ptr() as usize,
			Self::Color8Bit(tiles) => tiles.as_ptr() as usize,
		};

		(ptr as usize - 0x0601_0000) / core::mem::size_of::<Tile4bpp>()
	}
}


impl<'a, C: SpriteControl> SpriteRenderer<'a, C> {
	pub fn new(sprite_control: C, sprite_tiles: Volatile<&'a mut [u8]>) -> Self {
		let sprite_alloc = {
			let sprite_tiles = sprite_tiles.extract_inner();
			let layout = Layout::for_value(sprite_tiles);
			let raw_alloc = RawAlloc::new(sprite_tiles);

			RcAlloc(Rc::new(BuddyAlloc::new(layout, 64, raw_alloc).unwrap()))
		};

		SpriteRenderer {
			sprite_control,
			sprite_alloc,
			sprites: HashMap::new(),
		}
	}

	fn load_asset(&mut self, asset: &SpriteAsset) -> usize {
		let sprite_asset_id = asset as *const SpriteAsset<'_> as usize;

		if let Some(sprite_memory) = self.sprites.get(&sprite_asset_id) {
			sprite_memory.base_tile()
		} else {
			let sprite_memory = SpriteMemory::new(asset.tiles, self.sprite_alloc.clone());
			let base_tile = sprite_memory.base_tile();

			self.sprites.insert(sprite_asset_id, sprite_memory);

			base_tile
		}
	}
}

impl<C: SpriteControl> System for SpriteRenderer<'_, C> {
	fn run(&mut self, world: &mut World, _resources: &mut Resources) {
		render_sprites(world, self)
	}
}

////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////

pub fn render_sprites<C: SpriteControl>(world: &mut World, state: &mut SpriteRenderer<C>) {
	let mut attr_idx = busize::<0, 127>::new(0).unwrap();

	let query = [TypeId::of::<Position>(), TypeId::of::<Sprite>()];

	for entity in world.query(&query) {
		let position = world.get::<Position>(entity);
		let sprite = world.get::<Sprite>(entity);

		let base_tile = state.load_asset(sprite.asset);

		let color_mode = match sprite.asset.tiles {
			TilesRef::Color4Bit(_) => ColorMode::Color4Bit,
			TilesRef::Color8Bit(_) => ColorMode::Color8Bit,
		};

		let info = SpriteInfo::new_regular(base_tile, sprite.asset.shape_size, color_mode)
			.unwrap()
			.set_flip_mode(sprite.flip_mode)
			.set_palette_bank(busize::new(usize::from(sprite.palette_bank)).unwrap())
			.set_position(Vector2 {
				x: position.0.y.round().to_num(),
				y: position.0.y.round().to_num(),
			});

		state.sprite_control.set_sprite_info(attr_idx, info);

		attr_idx = match attr_idx.checked_add(1) {
			Some(val) => val,
			None => return,
		};
	}

	let mut affine_idx = busize::<0, 31>::new(0).unwrap();

	let query = [TypeId::of::<SpriteAffine>()];

	for entity in world.query(&query) {
		let sprite = world.get::<SpriteAffine>(entity);

		let position = Vector2::new(sprite.affine_matrix.m02, sprite.affine_matrix.m12);

		let base_tile = state.load_asset(sprite.asset);

		let color_mode = match sprite.asset.tiles {
			TilesRef::Color4Bit(_) => ColorMode::Color4Bit,
			TilesRef::Color8Bit(_) => ColorMode::Color8Bit,
		};

		let info = SpriteInfo::new_affine(
			base_tile,
			sprite.asset.shape_size,
			color_mode,
			affine_idx.get(),
		)
		.unwrap()
		.set_palette_bank(busize::new(usize::from(sprite.palette_bank)).unwrap())
		.set_position(Vector2 {
			x: position.x.round().to_num::<u16>() + 120,
			y: position.y.round().to_num::<u16>() + 80,
		});

		state.sprite_control.set_sprite_info(attr_idx, info);

		state
			.sprite_control
			.set_affine_matrix(affine_idx, &sprite.affine_matrix);

		affine_idx = match affine_idx.checked_add(1) {
			Some(val) => val,
			None => return,
		};
		attr_idx = match attr_idx.checked_add(1) {
			Some(val) => val,
			None => return,
		};
	}
}
