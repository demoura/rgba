// rgba <https://gitlab.com/demoura/rgba>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use core::any::TypeId;

use adven::{
	keypad::{Key, Keypad},
	math::{az::SaturatingAs, fixed::types::I16F16, num_traits::Zero, Matrix3, Vector2},
};
use adven_ecs::{storage::HashmapStorage, Component, Resources, World};

use crate::{
	components::PlayerMarker,
	physics::{AngularVelocity, Velocity},
	sprite::SpriteAffine,
	transform::{Position, Rotation, Scale, ViewTransformation},
};

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

#[derive(Clone, Copy)]
pub struct ShipStats {
	pub velocity: I16F16,
	pub max_health: u32,
	pub damage: u32,
	pub angular_velocity: I16F16,
}

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////


impl Component for ShipStats {
	type Storage = HashmapStorage<Self>;
}

////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////

pub fn player_velocity(world: &mut World, resources: &mut Resources) {
	let keypad = resources.get::<Keypad>();

	let query = [
		TypeId::of::<ShipStats>(),
		TypeId::of::<Velocity>(),
		TypeId::of::<PlayerMarker>(),
	];

	for entity in world.query(&query) {
		let stats = world.get::<ShipStats>(entity);
		let mut vel = world.get_mut::<Velocity>(entity);
		let rot = world.try_get::<Rotation>(entity);

		let base_vel = if keypad.key_pressed(Key::Down) {
			Vector2::new(I16F16::from_bits(0), I16F16::from_num(stats.velocity))
		} else if keypad.key_pressed(Key::Up) {
			Vector2::new(I16F16::from_bits(0), -I16F16::from_num(stats.velocity))
		} else {
			Vector2::zero()
		};

		if let Some(rot) = rot {
			vel.0 = Matrix3::rotation(rot.0).transform_point_fast(base_vel);
		} else {
			vel.0 = base_vel;
		}
	}
}

pub fn player_ang_velocity(world: &mut World, resources: &mut Resources) {
	let keypad = resources.get::<Keypad>();

	let query = [
		TypeId::of::<ShipStats>(),
		TypeId::of::<AngularVelocity>(),
		TypeId::of::<PlayerMarker>(),
	];

	for entity in world.query(&query) {
		let stats = world.get::<ShipStats>(entity);
		let mut vel = world.get_mut::<AngularVelocity>(entity);

		vel.0 = if keypad.key_pressed(Key::Right) {
			stats.angular_velocity
		} else if keypad.key_pressed(Key::Left) {
			-stats.angular_velocity
		} else {
			I16F16::from_bits(0)
		};
	}
}

pub fn update_sprite_affine(world: &mut World, resources: &mut Resources) {
	let view_matrix = resources.get::<ViewTransformation>();

	let query = [
		TypeId::of::<Position>(),
		TypeId::of::<Rotation>(),
		TypeId::of::<Scale>(),
		TypeId::of::<SpriteAffine>(),
	];

	for entity in world.query(&query) {
		let position = world.get::<Position>(entity);
		let rotation = world.get::<Rotation>(entity);
		let scale = world.get::<Scale>(entity);
		let mut sprite = world.get_mut::<SpriteAffine>(entity);

		let scale = Vector2 {
			x: I16F16::ONE / scale.0.x,
			y: I16F16::ONE / scale.0.y,
		};

		// let model = Matrix3::trs(position.0, -rotation.0, scale);
		let model =
			Matrix3::scale(scale) * Matrix3::rotation(-rotation.0) * Matrix3::translate(position.0);

		sprite.affine_matrix = (model * view_matrix.0).saturating_as();
	}
}
