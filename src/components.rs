// rgba <https://gitlab.com/demoura/rgba>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use adven_ecs::{Component, storage::HashmapStorage};

#[derive(Clone, Copy)]
pub struct PlayerMarker;

impl Component for PlayerMarker {
	type Storage = HashmapStorage<Self>;
}

