type BackgroundAlloc<'a> = RcAlloc<BuddyAlloc<RawAlloc<'a>>>;

pub enum TilesetMemory<'a> {
	Tileset4Bit(Vec<Tile4bpp, BackgroundAlloc<'a>>),
	Tileset8Bit(Vec<Tile8bpp, BackgroundAlloc<'a>>),
}

impl<'a> TilesetMemory<'a> {

	pub fn new(tiles: TilesRef<'_>, sprite_alloc: SpriteAlloc<'a>) -> Self {
		match tiles {
			TilesRef::Color4Bit(tiles) => {

				let mut sprite_img = Vec::new_in(sprite_alloc);

				sprite_img.extend_from_slice(&*tiles);

				Self::Color4Bit(sprite_img)
			},
			TilesRef::Color8Bit(tiles) => {
				let mut sprite_img = Vec::new_in(sprite_alloc);

				sprite_img.extend_from_slice(&*tiles);

				Self::Color8Bit(sprite_img)
			}
		}
	}

	pub fn color_mode(&self) -> ColorMode {
		match self {
			Self::Color4Bit(_) => ColorMode::Color4Bit,
			Self::Color8Bit(_) => ColorMode::Color8Bit,
		}
	}

	pub fn len(&self) -> usize {
		match self {
			Self::Color4Bit(tiles) => tiles.len(),
			Self::Color8Bit(tiles) => tiles.len(),
		}
	}

	pub fn base_tile(&self) -> usize {
		let ptr = match self {
			Self::Color4Bit(tiles) => tiles.as_ptr() as usize,
			Self::Color8Bit(tiles) => tiles.as_ptr() as usize,
		};

		(ptr as usize - 0x0601_0000) / core::mem::size_of::<Tile4bpp>()
	}
}

pub type TilemapMemory<'a> = Vec<TilemapMemory<'a>, BackgroundAlloc<'a>>;

pub struct TilemapRendererState<'a, C: SpriteControl> {
	sprite_control: C,
	background_alloc: BackgroundAlloc<'a>,
	tilesets: HashMap<usize, TilesetMemory<'a>>,
}

impl<'a, C: SpriteControl> TilemapRendererState<'a, C> {

	pub fn new(sprite_control: C, sprite_tiles: Volatile<&'a mut [u8]>) -> Self {
		let sprite_alloc = {
			let sprite_tiles = sprite_tiles.extract_inner();
			let layout = Layout::for_value(sprite_tiles);
			let raw_alloc = RawAlloc::new(sprite_tiles);

			RcAlloc(Rc::new(BuddyAlloc::new(layout, 64, raw_alloc).unwrap()))
		};

		SpriteRendererState {
			sprite_control,
			sprite_alloc,
			sprites: HashMap::new(),
		}
	}

	fn load_asset(&mut self, asset: &SpriteAsset) -> usize {
		let sprite_asset_id = asset as *const SpriteAsset<'_> as usize;

		if let Some(sprite_memory) = self.sprites.get(&sprite_asset_id) {
			sprite_memory.base_tile()
		} else {
			let sprite_memory = TilesetMemory::new(asset.tiles, self.sprite_alloc.clone());
			let base_tile = sprite_memory.base_tile();

			self.sprites.insert(sprite_asset_id, sprite_memory);

			base_tile
		}
	}
}


