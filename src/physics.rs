// rgba <https://gitlab.com/demoura/rgba>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Modules
////////////////////////////////////////////////////////////////////////////////

mod components;

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use core::any::TypeId;

use adven_ecs::{World, Resources};
pub use components::*;

use crate::transform::{Position, Rotation};

////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////

pub fn update_position(world: &mut World, _: &mut Resources) {

	let query = [
		TypeId::of::<Position>(),
		TypeId::of::<Velocity>(),
	];

	for entity in world.query(&query) {

		let mut pos = world.get_mut::<Position>(entity);
		let vel = world.get::<Velocity>(entity);

		pos.0 += vel.0;
	}

}

pub fn update_rotation(world: &mut World, _: &mut Resources) {

	let query = [
		TypeId::of::<Rotation>(),
		TypeId::of::<AngularVelocity>(),
	];

	for entity in world.query(&query) {

		let mut rot = world.get_mut::<Rotation>(entity);
		let vel = world.get::<AngularVelocity>(entity);

		rot.0 += vel.0;
	}
}
