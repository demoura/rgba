// rgba <https://gitlab.com/demoura/rgba>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#![no_std]
#![no_main]
#![feature(allocator_api, core_intrinsics, slice_ptr_get)]

#[cfg(feature = "gba")]
extern crate adven_bootloader;
extern crate alloc;

mod components;
mod game;
mod physics;
mod sprite;
mod transform;
/* mod tilemap; */

include!(concat!(env!("OUT_DIR"), "/assets.rs"));

use adven::{
	color::BGR5,
	display::{gba::*, *},
	keypad::Keypad,
	math::{az::SaturatingAs, busize, fixed::types::I16F16, num_traits::Zero, Matrix3, Vector2},
	register_bank::HardwareRegisterBank,
};
use adven_ecs::{Resources, Schedule, World};
use adven_macros::adven_asset;
use components::PlayerMarker;
use game::*;
use physics::{AngularVelocity, Velocity};
use sprite::*;
use transform::{CameraTransform, Position, Rotation, Scale, ViewTransformation};

#[no_mangle]
extern "C" fn main() -> ! {
	log::info!("Main called");

	log::info!("Starting I/O setup");

	let mut regbank = unsafe { HardwareRegisterBank::single_time_init() };

	let mut display_control = DisplayControl::new(regbank.display, regbank.vram);
	let mut keypad = Keypad::new(regbank.key_input.read());

	display_control.set_sprites_enabled(true);
	display_control.set_sprite_mapping(SpriteMappingMode::OneDimensional);

	regbank
		.bg_palette
		.map_mut(|palette| &mut palette[0])
		.write(BGR5::WHITE);

	let mut sprite_control = GbaSpriteControl::new(regbank.sprite_control);

	// Hide all sprites.
	for i in 0..128 {
		sprite_control.set_sprite_info(busize::new(i).unwrap(), SpriteInfo::new_hidden());
	}

	let mut tiled_vram = display_control.take_vram().unwrap();

	// 	let roguelike_tileset = adven_asset!("4a034037-b47b-41ba-9f81-8251c399b4ac");

	// 	match roguelike_tileset.tiles {
	// 		TilesRef::Color4Bit(_) => todo!(),
	// 		TilesRef::Color8Bit(tiles) => {
	// 			log::info!("Loading tilemap with {} tiles", tiles.len());

	// 			let mut bg_vram = tiled_vram
	// 				.map_mut(|vram| &mut vram.bg_tiles.borrow_mut_8bpp()[..]);

	// 			for (i, tile) in tiles.iter().enumerate() {
	// 				if (i < 512) {
	// 					bg_vram.index_mut(i).write(tile.clone());
	// 				}
	// 			}
	// 		},
	// 	}

	display_control.set_background_enabled::<Background0, RegularRenderMode>(true);

	let background = BackgroundControl::<RegularRenderMode>::new(
		0,
		31,
		ColorMode::Color8Bit,
		RegularBackgroundSize::Square32x32,
	)
	.unwrap();

	// 	display_control.set_background(Background0, background);

	// 	let level1_mini = adven_asset!("4461dd78-a1d9-4509-9060-aaeda40e3aeb");

	// 	{
	// 		let mut tilemap_entries = tiled_vram.map_mut(|vram| unsafe {
	// 			&mut core::mem::transmute::<&mut [u8; 64*1024], &mut [ScreenEntry; 32*1024]>(
	// 				vram.bg_tiles.borrow_mut_raw())[..]
	// 		});

	// 		let entries = level1_mini.0.iter()
	// 			.map(|metaentry| roguelike_tileset.metatiles[metaentry.tile_id()])
	// 			.enumerate();

	// 		for (i, entry) in entries {
	// 			if i < 1024 {
	// 				tilemap_entries.index_mut(31 * 1024 + i).write(entry);
	// 			}
	// 		}
	// 	}
	let sprite_vram = tiled_vram.map_mut(|vram| &mut vram.sprite_tiles.borrow_mut_raw()[..]);

	let mut sprite_renderer = SpriteRenderer::new(sprite_control, sprite_vram);

		log::info!("Finished I/O setup");

		log::info!("Starting Asset loading");

	let chess_palette_black = adven_asset!("ed3c9eaf-73df-4814-817d-6c2acd6e0081");
	for (i, color) in chess_palette_black.0.iter().enumerate() {
		regbank
			.sprite_palette
			.map_mut(|palette| &mut palette[i])
			.write(*color);
	}

	// 	let roguelike_palette = adven_asset!("aca4dd60-2bb7-4de6-9375-b38422525919");
	// 	for (i, color) in roguelike_palette.iter().enumerate() {
	// 		regbank
	// 			.bg_palette
	// 			.map_mut(|palette| &mut palette[i])
	// 			.write(*color);
	// 	}

	let mut world = World::default();

	world.register::<Position>();
	world.register::<Rotation>();
	world.register::<Scale>();

	world.register::<Velocity>();
	world.register::<AngularVelocity>();

	world.register::<Sprite>();
	world.register::<SpriteAffine>();

	world.register::<ShipStats>();
	world.register::<PlayerMarker>();

	log::info!("Created World");

	// 	let king_sprite = adven_asset!("a5948639-aa95-4633-9475-da638a168bbb");

	// 	let king = world.push((
	// 		Position(Vector2::new(I16F16::from_num(20i32), I16F16::from_num(7i32))),
	// 		Rotation(I16F16::from_bits(0i32)),
	// 		Scale(Vector2::new(I16F16::from_num(1i32), I16F16::from_num(1i32))),
	// 		Velocity(Vector2::zero()),
	// 		SpriteAffine {
	// 			asset: king_sprite,
	// 			palette_bank: PaletteBankIdx::new(0).unwrap(),
	// 			affine_matrix: Matrix3::rotation(I16F16::from_num(64i32)).saturating_as(),
	// 		},
	// 	));

	// 	log::info!("Created King");

	let ship = adven_asset!("a228389a-b357-4394-ab00-cb7a2e87d39d");

	let player_ship = world.create_entity();

	world.insert(player_ship, PlayerMarker).unwrap();
	world
		.insert(
			player_ship,
			ShipStats {
				velocity: ship.velocity,
				max_health: ship.max_health,
				damage: ship.damage,
				angular_velocity: ship.angular_velocity,
			},
		)
		.unwrap();
	world
		.insert(
			player_ship,
			Position(Vector2::new(I16F16::from_num(0i32), I16F16::from_num(0i32))),
		)
		.unwrap();
	world
		.insert(player_ship, Rotation(I16F16::from_bits(0)))
		.unwrap();
	world
		.insert(
			player_ship,
			Scale(Vector2::new(I16F16::from_num(1i32), I16F16::from_num(1i32))),
		)
		.unwrap();
	world
		.insert(player_ship, Velocity(Vector2::zero()))
		.unwrap();
	world
		.insert(player_ship, AngularVelocity(I16F16::from_bits(0i32)))
		.unwrap();
	world
		.insert(
			player_ship,
			SpriteAffine {
				asset: &*ship.sprite,
				palette_bank: PaletteBankIdx::new(0).unwrap(),
				affine_matrix: Matrix3::rotation(I16F16::from_num(64i32)).saturating_as(),
			},
		)
		.unwrap();

	log::info!("Created Ship");

	log::info!("Finished Asset loading");

	log::info!("Starting Legion setup");

	let mut resources = Resources::default();

	resources.insert(display_control.vertical_count());
	resources.insert(CameraTransform(player_ship));
	resources.insert(ViewTransformation::default());

	let mut vdraw_schedule = Schedule::default()
		.with_system(game::player_velocity)
		.with_system(game::player_ang_velocity)
		.with_system(physics::update_position)
		.with_system(physics::update_rotation)
		.with_system(transform::view_transform)
		.with_system(game::update_sprite_affine);

	let mut vblank_schedule = Schedule::default().with_system(sprite_renderer);

	log::info!("Finished Legion setup");

	log::info!("Starting Game loop");

	loop {
		// Update keypad.
		keypad.update(regbank.key_input.read());

		// Run game systems.
		resources.insert(keypad);
		vdraw_schedule.run(&mut world, &mut resources);

		// Wait for vblank.
		while display_control.vertical_count().read() <= VBLANK_START {}

		// Update display controller
		display_control.commit();
		// regbank.display_control.write(display_control.register());

		// Update object attributes
		// Update sprite and tile data.
		if display_control.sprites_enabled() {
			// render_sprites(&mut world, &mut sprite_rendererer);
			vblank_schedule.run(&mut world, &mut resources);
		}
	}
}
