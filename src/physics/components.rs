// rgba <https://gitlab.com/demoura/rgba>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use adven::math::{fixed::types::I16F16, Vector2};
use adven_ecs::{storage::HashmapStorage, Component};

#[derive(Clone, Copy, PartialEq)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Velocity(pub Vector2<I16F16>);

#[derive(Clone, Copy, PartialEq)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct AngularVelocity(pub I16F16);

impl Component for Velocity {
	type Storage = HashmapStorage<Self>;
}

impl Component for AngularVelocity {
	type Storage = HashmapStorage<Self>;
}
