// rgba <https://gitlab.com/demoura/rgba>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Modules
////////////////////////////////////////////////////////////////////////////////

mod components;

use adven::math::{fixed::types::I16F16, Matrix3, Vector2};
use adven_ecs::{Entity, Resources, World};
pub use components::*;

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

pub struct CameraTransform(pub Entity);

pub struct ViewTransformation(pub Matrix3<I16F16>);

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

impl Default for ViewTransformation {
	fn default() -> Self {
		Self(Matrix3::identity())
	}
}

////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////

pub fn view_transform(world: &mut World, resources: &mut Resources) {

	let camera_transform = resources.get::<CameraTransform>();
	let mut view_transform = resources.get_mut::<ViewTransformation>();

	let position = world.get::<Position>(camera_transform.0);
	let rotation = world.get::<Rotation>(camera_transform.0);

	view_transform.0 = Matrix3::trs(-position.0, rotation.0, Vector2::one());
}
