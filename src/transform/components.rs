// rgba <https://gitlab.com/demoura/rgba>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use adven::math::{Vector2, fixed::types::I16F16};
use adven_ecs::{storage::HashmapStorage, Component};

#[derive(Clone, Copy, Eq, PartialEq)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Position(pub Vector2<I16F16>);

#[derive(Clone, Copy, Eq, PartialEq)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Rotation(pub I16F16);

#[derive(Clone, Copy, Eq, PartialEq)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Scale(pub Vector2<I16F16>);

impl Component for Position {
	type Storage = HashmapStorage<Self>;
}

impl Component for Rotation {
	type Storage = HashmapStorage<Self>;
}

impl Component for Scale {
	type Storage = HashmapStorage<Self>;
}
